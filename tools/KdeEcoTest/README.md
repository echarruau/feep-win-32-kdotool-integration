# KdeEcoTest

Contents
========

* [Why?](#why)
* [Installation](#installation)
* [Usage](#usage)
* [Want to contribute?](#want-to-contribute)


### Why?

A Standard Usage Scenario reflects the typical functions of an application and is central to measuring the energy consumption of software in a reproducible way.

`KdeEcoTest` helps to create a script which simulates the activities of a normal user in order to create a Standard Usage Scenario. `KdeEcoTest` also runs those scripts to automate emulation of user behavior in order to measure energy consumption of an application while in use.

`KdeEcoTest` is a CLI based Python tool that supports running on Linux and Windows. KdeEcoTest on Linux requires running on KDE plasma as it uses APIs provided by the KWin compositor.

  
### Testing and Usage

#### Setup: For Linux
`KdeEcoTest on Linux requires running on KDE plasma as it uses APIs provided by the KWin compositor`.
We recommend using a distro that comes natively with KDE plasma, rather than installing it as a separate desktop environment. This may cause dependency issues.

To run on linux you require the following to be installed on your system - `make`, `libdbus`, `cmake`, `pkg-config`, `pip`, `rust` and `cargo`. You also require the `python-dev` package to build the required python dependencies.
To install rust, we highly recommend the rust installation guide found here https://www.rust-lang.org/tools/install, it also comes with cargo package manager.

```bash
# Install python-dev package
$ sudo apt install python3-dev
# On fedora
$ sudo dnf install python3-devel

# Install make
$ sudo apt install make

# Install cmake
$ sudo apt install cmake

# Install pip
$ sudo apt install python3-pip

# Install pkg-config
$ sudo apt install pkg-config
# on fedora
$ sudo dnf install pkgconf-pkg-config

# Install libdbus-dev
$ sudo apt install libdbus-1-dev
$ sudo apt install libdbus-glib-1-dev
# on fedora
$ sudo dnf install dbus-devel
$ sudo dnf install dbus-glib-devel
```
Python requirements for running KdeEcoTest are meant to be installed using `pipenv`. We recommend installing pipenv using pip, (pipenv in the apt repository is outdated, and may lead to issues), and then adding ~/.local/bin to the path (where it is installed). Follow this guide if you face any issues https://pipenv.pypa.io/en/latest/installation.html.

```bash
# Installing pipenv
$ pip install pipenv
$ export PATH="~/.local/bin":$PATH
```
KdeEcotest uses libevdev for reading and simulating events from input devices, and requires permission to read and write from `/dev/input`, `/dev/uinput`, additionally it also requires permission to write from `/dev/console` for getting keyboard layout using dumpkeys utility.

These permission can be given either by modifying the permissions to the file or adding the user to the required groups.


To grant permissions temporarily (Per login session)

```bash
$ sudo chmod +0666 /dev/uinput
$ sudo chmod -R +0666 /dev/input
$ sudo chmod +0666 /dev/console
```

For permanently giving permissions, you can add the user to groups input and tty (input group has permission to read and write from /dev/input and tty group has permission to write from /dev/cosole), for /dev/uinput, you will have to add new rules. (This may require restarting the system to take effect)

```bash
$ sudo usermod -aG input,tty $USER

# For adding to rules for uinput
# Create a group
$  sudo  groupadd  -r  uinput
# Add yourself to the group
$  sudo  usermod  -aG  uinput  $USER
# Give the group permissions to use the uinput kernel module
$  echo  'KERNEL=="uinput", MODE="0660", GROUP="uinput", OPTIONS+="static_node=uinput"' \| sudo  tee  /etc/udev/rules.d/60-uinput.rules
```

##### Clone the repo and setup the directory

```bash
$ git clone https://invent.kde.org/echarruau/feep-win-32-kdotool-integration.git
$ cd  feep/tools/KdeEcoTest/

# Do these steps for running on Linux
$ mkdir -p externals/kdotool
$ git clone -b dev https://github.com/krathul/kdotool.git  externals/kdotool
$ make

# Create environment and install dependencies
$ pipenv install
# Activate the environment
$ pipenv shell
```

#### Setup: For Windows

```bash
$ git clone https://invent.kde.org/echarruau/feep-win-32-kdotool-integration.git
$ cd feep/tools/KdeEcoTest/
$ pip install  pipenv

# Create environment and install dependencies
$ pipenv  install

# Activate the environment
$ pipenv  shell
```

### Running the tool

#### Create new script

```bash
$ python3 KdeEcoTest.py create <SCRIPT_NAME>
```
- A round mouse pointer appears. Click on the window you want to test. Now, you can use these options provided by `KdeEcoTest` to simulate actions and create a test script.

- To abort the program : Press Esc

```shell

Commands:

dw:  define  window.
ac:  add clicks.
sc:  stop  add  clicks.
ws:  write  to  the  screen.
wtl:  write  test  timestamp  to  log.
wmtl:  write  message  to  log.

```

#### Run a script

`On X11 before running the script set your mouse accelaration profile to flat`
System settings > Input devices > Mouse

`KdeEcoTest` automates the actions stored in the created scripts.
To run `KdeEcoTest` script:

```bash
$ python3 KdeEcoTest.py run <SCRIPT_NAME>

```

### Want to contribute?

Before contributing, fork the repository and make an MR when you fix the issue.
Contribute in the following ways:

#### To-Do's

See "KdeEcoTest" Issues at this repository's [issue tracker](https://invent.kde.org/teams/eco/feep/-/issues).

  

Ask questions at the Matrix room [KDE Energy Efficency](https://matrix.to/#/#energy-efficiency:kde.org).
