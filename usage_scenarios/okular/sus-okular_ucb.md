# General Information

**Software**: KDE Okular

**Product Group:** PDF & Universal Document Reader

**Version:** Okular 21.12.3

**Script Name:** *sus-okular_ucb.sh*

**Script Author(s):** Joseph P. De Veaugh-Geiss (KDE), Aakarsh MJ (KDE)

**Script Details:** The script attempts to recreate as closely as possible the Actiona script by Franziska Mai (Umwelt Campus Birkenfeld). For the original Action script, see:

<https://invent.kde.org/teams/eco/feep/-/blob/master/measurements/okular/2020-2021/standardnutzungsszenarioKDEOkular.ascr>

**Load Generator:** *xdotool* (GNU/Linux)

**System Under Test (SUT):** Ubuntu 22.04.2 LTS (Jammy Jellyfish)

**Screen Resolution:** 1920x1080~60Hz

**Note**: The `pandoc` command to generate an ODT version of this document is as follows:

    $ pandoc -t odt -s sus-okular_ucb.md -s -o sus-okular_ucb.odt

# Set Up

 - Actions marked with \*\* are custom commands and need to be setup by you. To set custom commands, in Okular go to *Settings* > *Configure Keyboard Shortcuts …* and then search for the task and add following shortcuts:

    1.  -   Rotate Left: `Alt+l`
        -   Rotate Right: `Alt+r`
        -   Invert color: `Alt+i`
        -   Fit to width: `Ctrl+Shift+w`

 - The first command in the script will automatically set keyboard to English (US).

 - Previous CSV logs on the date of execution (YYYYMMDD_log_sus_okular.csv) will be deleted at the beginning of the script.

 - To start each run with a clean slate, at the beginning of the script and at the end of each iteration these files are deleted:

    1.  -   `~/.config/okularrc`
        -   `~/.config/okularpartrc`
        -   `~/.local/share/okular/*`
        -   `~/20yearsofKDE.pdf`

 - Moreover, at the beginning of each iteration the PDF file *20yearsofKDE.pdf* will be copied into the home directory.

 - Also at the beginning of each iteration there is a 60 second burn-in time.

Running The SUS:

 1. Install *Okular* and *xdotool* (<https://github.com/jordansissel/xdotool>) on System Under Test (SUT)

 2. Make a directory `~/Documents/okular/`

	```
    $ mkdir ~/Documents/okular/
    ```

 3. Download Okular SUS script into *Downloads* directory:

    <https://invent.kde.org/teams/eco/feep/-/tree/master/usage_scenarios/okular/sus-okular_ucb.sh>

 4. Download PDF "20yearsofKDE.pdf" into *Downloads* directory: <https://20years.kde.org/book/20yearsofKDE.pdf>

 5. Copy SUS script and PDF into *~/Documents/okular/*

	```
    $ cp ~/Downloads/20yearsofKDE.pdf ~/Documents/okular/20yearsofKDE.pdf
	```
	```
    $ cp ~/Downloads/sus-okular_ucb.sh ~/Documents/okular/sus-okular_ucb.sh
	```

 6. Change directory to *~/Documents/okular/*

	```
    $ cd ~/Documents/okular/
	```
	
 7. Give executable permission to the *sus-okular_ucb.sh* file

	```
    $ chmod +x sus-okular_ucb.sh
	```

 8. Run the script

	```
    $ ./sus-okular_ucb.sh
	```
	
 9. Two log files are generated and saved in the home directory when running the SUS:

    1. The CSV log file of actions, which will have the date appended to the file name (~ *YYYYMMDD_log_sus_okular.csv*)
    2. A text file with the information about the version of Okular tested and SUT (~/*YYYYMMDD_system-info.txt*)

# Workflow Of Actions

| No | Time Elapsed (in seconds) | Job Description | Action |
|---|---|---|---|
| At start (once) | na | - Quit Okular if running | `$ killall okular` |
| | | - Remove previous logs and dot-files if present | `$ rm -f ~/$(date -d "today" +"%Y%m%d")_log_sus_okular.csv` <br> `$ rm -f ~/.config/okularrc` <br> `$ rm -f ~/.config/okularpartrc` <br> `$ rm -f -r ~/.local/share/okular/*` <br> `$ rm -f ~/20yearsofKDE.pdf` |
| Before each run | na | - Copy PDF file into home | `$ cp ~/Documents/okular/20years ofKDE.pdf ~/20yearsofKDE.pdf` |
| Burn-in 60 seconds | na | | |
| 00 | 0 s | - Start iteration (01 – 30) | Log: "startTestrun" |
| 01 a | 1 s | - Open PDF document | `$ okular ~/20yearsofKDE.pdf` |
| 01 b | 2 s | - Fit to width | \*\*Ctrl+Shift+w\*\* |
| 02 a | 4 s | - Open 'Go to' dialogue | Ctrl+g |
| 02 b | | - Type 38 + Return | Type: "38" <br> Return |
| 03 a | 7 s | - Toggle annotation panel | F6 |
| 03 b | 11 s | - Toggle highlighter tool, select text to highlight | Alt+1 |
| 03 c | 13 s | - Write annotation | Type: "Very interesting text! I should read more about this topic." |
| 03 d | 21 s | - Toggle highlighter tool again to return to browsing mode | Alt+1 |
| 04 a | 23 s | - Start presentation mode | Ctrl+Shift+p |
| 04 b | 25 s | - Move down 5 pages | Down arrow x 5 |
| 04 c | 34 s | - Move up 5 pages | Up arrow x 5 |
| 04 d | 43 s | - Exit presentation mode | Escape |
| 05 a | 47 s | - Rotate page right twice | \*\*Alt+r\*\* x 2 |
| 05 b | 59 s | - Rotate page left twice | \*\*Alt+l\*\* x 2 |
| 06 a | 71 s | - Move forward 5 pages | Right arrow x 5 |
| 06 b | 81 s | - Move backward 5 pages | Left arrow x 5 |
| 07 a | 92 s | - Zoom to 100% | Ctrl+0 |
| 07 b | 95 s | - Zoom to 400% | Ctrl++ x 5 |
| 07 c | 100 s | - Fit to width | \*\*Ctrl+Shift+w\*\* |
| 08 | 101 s | - Invert colors | \*\*Alt+i\*\* |
| 09 a | 106 s | - Open 'Go to' dialogue | Ctrl+g |
| 09 b | | - Type 42 + Return | Type: "42" <br> Return |
| 10 a | 110 s | - Toggle annotation panel | F6 |
| 10 b | 114 s | - Toggle highlighter tool, select text to highlight | Alt+1 |
| 10 c | 116 s | - Write annotation | Type: "Again this is very interesting, should read more." |
| 10 d | 124 s | - Toggle highlighter tool again to return to browsing mode | Alt+1 |
| 11 a | 125 s | - Start presentation mode | Ctrl+Shift+p |
| 11 b | 146 s | - Exit presentation mode | Escape |
| 12 a | 148 s | - Rotate page right twice | \*\*Alt+r\*\* x 2 |
| 12 b | 160 s | - Rotate page left twice | \*\*Alt+l\*\* x 2 |
| 13 a | 173 s | - Move forward 5 pages | Right arrow x 5 |
| 13 b | 183 s | - Move backward 5 pages | Left arrow x 5 |
| 14 a | 193 s | - Zoom to 100% | Ctrl+0 |
| 14 b | 196 s | - Zoom to 400% | Ctrl++ x 5 |
| 14 c | 202 s | - Fit to width | \*\*Ctrl+Shift+w\*\* |
| 15 | 203 s | - Invert colors back | \*\*Alt+i\*\* |
| 16 a | 208 s | - Save PDF | Ctrl+s |
| 16 b | 209 s | - Quit Okular | Ctrl+q |
| 17 | 210 s | - End iteration (01 – 30) | "stopTestrun" |
| Each iteration end | na | - Clean up | `$ rm ~/.config/okularrc` <br> `$ rm ~/.config/okularpartrc` <br> `$ rm -r ~/.local/share/okular/*` <br> `$ rm ~/20yearsofKDE.pdf` <br> |

